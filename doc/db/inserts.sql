
INSERT INTO Users
  (userDeleted,       userLogin, userFullName,     userPasswordHash) VALUES
  (          0, "Administrator",           '', SHA("Administrator"));

INSERT INTO Roles
  ( roleID,     roleName ) VALUES
  (      1, "superadmin" ),
  (      2,      "admin" );

INSERT INTO UsersRoles ( userID, roleID ) VALUES ( 1, 1 ), ( 1, 2 );

