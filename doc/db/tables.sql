SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `some_nameDb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `some_nameDb`;

-- -----------------------------------------------------
-- Table `some_nameDb`.`Users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `some_nameDb`.`Users` ;

CREATE  TABLE IF NOT EXISTS `Users` (
  `userID` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `userDeleted` TINYINT(1) NOT NULL DEFAULT 1 ,
  `userLogin` VARCHAR(255) NOT NULL ,
  `userPasswordHash` VARCHAR(255) NOT NULL ,
  `userFullName` VARCHAR(255) NULL ,
  `userFirstName` VARCHAR(255) NULL ,
  `userLastName` VARCHAR(255) NULL ,
  `userPhone` VARCHAR(255) NULL ,
  `userEmail` VARCHAR(255) NULL ,
  PRIMARY KEY (`userID`) ,
  UNIQUE INDEX `uidx_userLogin` (`userLogin` ASC) ,
  INDEX `idx_userDeleted` (`userDeleted` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `some_nameDb`.`Roles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `some_nameDb`.`Roles` ;

CREATE  TABLE IF NOT EXISTS `Roles` (
  `roleID` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `roleName` VARCHAR(255) NOT NULL ,
  PRIMARY KEY (`roleID`) ,
  UNIQUE INDEX `uidx_roleName` (`roleName` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `some_nameDb`.`UsersRoles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `some_nameDb`.`UsersRoles` ;

CREATE  TABLE IF NOT EXISTS `UsersRoles` (
  `userID` INT UNSIGNED NOT NULL ,
  `roleID` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`userID`, `roleID`) ,
  INDEX `fk_UsersRoles_userID` (`userID` ASC) ,
  INDEX `fk_UsersRoles_roleID` (`roleID` ASC) ,
  CONSTRAINT `fk_UsersRoles_userID`
    FOREIGN KEY (`userID` )
    REFERENCES `Users` (`userID` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_UsersRoles_roleID`
    FOREIGN KEY (`roleID` )
    REFERENCES `Roles` (`roleID` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
