<?php

/**
 * @file doc/documentation.php
 *
 * @brief Documentation-only file.
 *
 * This file's only purpose is to gather documentation that should be used
 * by Doxygen, but does not fit anywhere else.
 */


/**
 *  @mainpage
 *
 *  @section introduction Introduction
 *
 *  Project description
 *
 *  @section copyright Copyright notice
 *  Unless otherwise stated, contents of this documentation are private
 *  property of flux inc. and are not allowed to be copied or published
 *  without flux inc. written permission.
 *
 *  Copyright 2008 flux inc.
 */

?>
