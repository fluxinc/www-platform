<?php

function ifndefdefine( $constant, $value ) {
  if ( ! defined( $constant ) )
    define( $constant, $value );
}

// Load global configuration
require_once( 'configuration.inc.php' );

?>
