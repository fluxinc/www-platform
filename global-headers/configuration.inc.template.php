<?php

ifndefdefine( 'APP_SHORT', 'some_name' );
ifndefdefine( 'APP_ROOT', '/pathToProject/' );

// Paths
ifndefdefine( 'RPC_CLASSES_PATH', APP_ROOT . 'rpc-classes/');
ifndefdefine( 'RPC_HEADERS_PATH', APP_ROOT . 'rpc-headers/');
ifndefdefine( 'SERVICES_PATH', APP_ROOT . 'services/' . APP_SHORT . '/' );
ifndefdefine( 'UI_CLASSES_PATH', APP_ROOT . 'ui-classes/');
ifndefdefine( 'UI_HEADERS_PATH', APP_ROOT . 'ui-headers/');
ifndefdefine( 'THIRD_PARTY_PATH', APP_ROOT . '3rd-party/');
ifndefdefine( 'CACHE_PATH', APP_ROOT . 'cache/' );
ifndefdefine( 'LOGS_PATH', APP_ROOT . 'logs/' );
ifndefdefine( 'UPLOAD_PATH', APP_ROOT . 'uploads/' );

// Database ifndefdefines
ifndefdefine('DATABASE_SERVER', 'localhost');
ifndefdefine('DATABASE_NAME', APP_SHORT . 'Db');
ifndefdefine('DATABASE_USERNAME', APP_SHORT . 'user');
ifndefdefine('DATABASE_PASSWORD', 'testpwd');
ifndefdefine('TABLE_PREFIX','');

// User interface configuration
ifndefdefine('SMARTY_DIR', THIRD_PARTY_PATH . 'smarty_libs/');
ifndefdefine('SMARTY_WORKDIR', UI_HEADERS_PATH . 'smarty/' );
ifndefdefine('LANG', 'en');

// Helpers configuration
ifndefdefine( 'PAYPAL_EMAIL', 'seller@paypalsandbox.com' );
ifndefdefine( 'PAYPAL_CURRENCY', 'USD' );
ifndefdefine( 'MAILER_NAME', 'Flux Inc. Mailer' );

// Logging
require_once( RPC_CLASSES_PATH . 'GenericHelper.php' );
ifndefdefine('LOGGING_LEVEL',
  GenericHelper::LOG_INFO | GenericHelper::LOG_DEBUG
);

// Debug mode
@include('override-debug.inc.php');
ifndefdefine( 'FLUX_INC_DEBUG_MODE', false );

// Php config
date_default_timezone_set( 'America/Toronto' );

?>
