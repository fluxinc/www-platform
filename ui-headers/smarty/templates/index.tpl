{if $loggedIn instanceof Error}
  Call to Users::isLoggedIn() returned error. Please check your installation.
{elseif false === $loggedIn}
  Welcome to our new project. You should now be able to log in using
  <a href="amfphp/browser/">amfphp browser</a>.
{else}
  Welcome to our new project. You are now logged in.
{/if}
