<?php

require_once( THIRD_PARTY_PATH . 'smarty_libs/Smarty.class.php' );

class FluxIncSmarty extends Smarty {
  function __construct() {
    $this->template_dir = SMARTY_WORKDIR . 'templates';
    $this->config_dir = SMARTY_WORKDIR . 'configs';

    $this->compile_dir = CACHE_PATH . 'smarty/templates_c';
    $this->cache_dir = CACHE_PATH . 'smarty/cache';

    $this->debugging = FLUX_INC_DEBUG_MODE;
  }
}

?>
