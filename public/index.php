<?php

require_once( 'includes/header.inc.php' );

$users = new ServiceCaller( 'Users' );
$loggedIn = $users->isLoggedIn();

$smarty = new FluxIncSmarty();
$smarty->assign( 'loggedIn', $loggedIn );
$smarty->display('index.tpl');

?>
