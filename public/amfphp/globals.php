<?php

	//This file is intentionally left blank so that you can add your own global settings
	//and includes which you may need inside your services. This is generally considered bad
	//practice, but it may be the only reasonable choice if you want to integrate with
	//frameworks that expect to be included as globals, for example TextPattern or WordPress

	//Set start time before loading framework
	list($usec, $sec) = explode(" ", microtime());
	$amfphp['startTime'] = ((float)$usec + (float)$sec);

    require_once( 'require-globals.php' );
    require_once( RPC_HEADERS_PATH . 'globals.php' );

    preg_match( '@^(.*[/\\\\])[^/\\\\]+[/\\\\]$@', SERVICES_PATH, $matches );
    $voPath = $servicesPath = $matches[ 1 ];
    session_start();

?>
