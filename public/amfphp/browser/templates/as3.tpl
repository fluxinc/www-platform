<!--
////////////////////////////////////////////////////////////////////////////////
// This file gernated by AMFPHP 1.2
// You can get AMFPHP to generate code customized to your preferences
// By modifying the /browser/templates/as3.tpl file
////////////////////////////////////////////////////////////////////////////////
-->
<mx:Script>
   <![CDATA[
    import flash.net.Responder;
 	public var gateway : RemotingConnection;
	public var gatewayUrl:String = "<?php echo $info['gatewayUrl']; ?>" ;
	// Define all variables that are used for remoting call arguments.
<?php foreach($info['methods'] as $key => $method) { ?>
	// <?php echo $info['package'].$info['class'] ?>.<?php echo $method['methodName']?> has arguments: <?php echo $method['typedArgs']?> 
<?php } ?>	
	public function initApplication():void {
		gateway = new RemotingConnection(gatewayUrl);
	<?php foreach($info['methods'] as $key => $method) { ?>
	//<?php echo $method['description']?>
		
		gateway.call( "<?php echo $info['package'].$info['class'] ?>.<?php echo $method['methodName']?>", new Responder(handle<?php echo $method['methodName']?>, onFault), <?php echo $method['typedArgs']?>);
	<?php } ?>
}
	<?php foreach($info['methods'] as $key => $method) { ?>
// call back handler for <?php echo $info['package'].$info['class'] ?>.<?php echo $method['methodName']?>

	public function handle<?php echo $method['methodName']?>( result : Object ) :void 
	{
		// TextInput isntance to display the result.
		<?php echo $method['methodName']?>Output.text = String(result);
	}
	<?php } ?>
public function onFault( fault : Object ):void
	{
		// TextInput isntance to display the result. Add your own code here
		output.text = String(fault);
	}
	]]>
</mx:Script>





