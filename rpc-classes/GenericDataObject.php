<?php

/**
 *  @defgroup dos Data Objects
 *  @brief Data returned by services.
 *
 *  @ingroup Helpers
 */

/**
 *  @brief A class that should be inherited by all data objects returned from
 *  services.
 *
 *  @ingroup dos
 */
class GenericDataObject extends GenericHelper {
  public $_explicitType = null;

  public function __construct() {
    $this->_explicitType = get_class( $this );
  }
  /**
   *  @brief Format date received from db to what client expects.
   *
   *  Currently this does nothing.
   */
  protected function dbDateToClientFormat( $date ) {
    return $date;
  }

  /**
   *  @brief Format date time received from db to what client expects.
   *
   *  Currently this does nothing.
   */
  protected function dbDatetimeToClientFormat( $date ) {
    return $date;
  }


}

?>
