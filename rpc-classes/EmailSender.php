<?php

/**
 * @brief Helps with sending emails.
 * @details All emails are cached and sent when object's instance dies.
 */
class EmailSender extends GenericHelper {
  private $cachedEmails = array();

  public function send(
    $recipients, $subject, $message, $mime = 'text/plain'
  ) {
    if ( empty( $recipients ) ) {
      return false;
    }

    if ( is_array( $recipients ) ) {
      $numberOfRecipients = count( $recipients );
      $recipients = implode( $recipients, ', ' );
    }
    else {
      $numberOfRecipients = 1;
    }

    $subject = '=?utf-8?b?' . base64_encode( $subject ) . '?=';
    $mailer = '=?utf-8?b?' . base64_encode( MAILER_NAME ) . '?=';

    if ( $numberOfRecipients === 1 ) {
      $headers  = "From: $mailer <no-reply@fluxinc.ca>\n";
      $headers .= "MIME-Version: 1.0\n";
      $headers .= "Content-Type: $mime; charset=\"utf-8\"";
    }
    else {
      $headers  = "From: $mailer <no-reply@fluxinc.ca>\n";
      $headers .= "MIME-Version: 1.0\n";
      $headers .= "Bcc: $recipients\n";
      $headers .= "Content-Type: $mime; charset=\"utf-8\"";
      $recipients = '';
    }

    $this->cachedEmails[] = array(
      'recipients' => $recipients,
      'subject' => $subject,
      'message' => $message,
      'headers' => $headers
    );
    return true;
  }

  public function __destruct() {
    foreach ( $this->cachedEmails as $email ) {
      mail(
        $email[ 'recipients' ],
        $email[ 'subject' ],
        $email[ 'message' ],
        $email[ 'headers' ]
      );
    }
  }
}

?>
