<?php
/*
 *  This API was originally found in Authenticate class - part of utilities. It was
 *  rewritten for this project.
 *
 *  The original copyright states:
 *  Copyright (c) 2003 amfphp.org
 *  Licensed under GNU Public License (http://opensource.org/licenses/gpl-license.php)
 *
 *  Here goes the rewritten code.
 */

// Start session
if( ! session_id() ) {
  session_start();
}

/**
 * @brief Defines helper methods related to authentication.
 */
class Authenticate {
  /**
   *  @brief Tells whether there is currently logged in user.
   *
   *  @return Bool.
   */
  public static function isAuthenticated () {
    return (bool) isset( $_SESSION[ APP_SHORT . '_username' ] );
  }

  /**
   *  @brief Returns the current user id.
   *
   *  Throws exception if there is no user currently logged in.
   *
   *  @return Value that was passed to login method.
   */
  public static function getAuthUser () {
    if( ! isset( $_SESSION[ APP_SHORT . '_username' ] ) )
      throw new Exception( 'authenticate-unknown-user' );

    return $_SESSION[ APP_SHORT . '_username'];
  }

  /**
   *  @brief Checks if user belongs to at least one of the groups passed as CSV.
   *
   *  Roles are case insensitive.
   *
   *  @param $roles comma delimited list of roles
   *  @return bool Whether the user is in at least one of the roles.
   */
  public static function isUserInRole($roles) {
    if ( empty( $roles ) )
      return true;

    $roles = strtolower( $roles );

    if ( ! isset( $_SESSION[ APP_SHORT . '_roles' ] ) ) {
      if ( 'guest' === $roles ) {
        return true;
      }
      else {
        return false;
      }
    }

    $intersect = array_intersect(
      explode( ',', $roles ),
      explode( ',', $_SESSION[ APP_SHORT . '_roles' ] )
    );
    return (bool) count( $intersect );
  }

  /**
   *  @brief Assumes that the user has verified the credentials and logs in the
   *  user.
   *
   *  The login method hides the session implementation for storing the user
   *  credentials.
   *
   *  @param $id Value identifying the user. May be id or login.
   *  @param $roles The comma delimited list of users roles (case-insensitive)
   */
  public static function login( $id, $roles ) {
    if( ! session_id() ) {
      session_start();
    }
    $_SESSION[APP_SHORT . '_username'] = $id;
    $_SESSION[APP_SHORT . '_roles'] = strtolower( $roles );

    return true;
  }

  /**
   *  @brief Kills the user session and terminates the login properties.
   */
  public static function logout() {
    unset( $_SESSION[ APP_SHORT . '_username' ] );
    unset( $_SESSION[ APP_SHORT . '_roles' ] );
    return true;
  }
}

?>
