<?php

/**
 * @brief Handles Instant Payment Notifications from PayPal.
 */
class PayPalMgmt extends GenericMgmt {
  ///@privatesection

  const QUERY_INSERT_TRANSACTION =
    'INSERT INTO Transactions
      ( trxExternalId, trxItemName, trxType, trxPaymentStatus, trxAmount )
    VALUES
      ( :id, :item, :type, :status, :amount )';
  const QUERY_INSERT_DETAILS =
    'INSERT INTO TransactionsDetails ( trxID, trxdProperty, trxdValue )
    VALUES %VALUES';


  public function __construct() {
    parent::__construct();
  }

  public function insertTransaction( $ppId, $item, $type, $status, $amount ) {
    $ppId = trim( $ppId );
    if ( empty( $ppId ) ) {
      throw new WrongDataException( 'paypal-transaction-id' );
    }

    $stmt = $this->dbh->prepare( self::QUERY_INSERT_TRANSACTION );
    $stmt->bindValue( ':id', $ppId );
    $stmt->bindValue( ':item', $item );
    $stmt->bindValue( ':type', $type );
    $stmt->bindValue( ':status', $status );
    $stmt->bindValue( ':amount', $amount );
    $stmt->execute();
    return $this->dbh->lastInsertId();
  }

  public function insertDetails( $trxId, $properties ) {
    $values = array_fill( 0, count( $properties ), ' ( ?, ?, ? )' );
    $values = implode( ',', $values );
    $values = array( '%VALUES' => $values );

    $stmt = $this->dbh->prepare( self::QUERY_INSERT_DETAILS, $values );

    $position = 1;
    foreach( $properties as $key => $val ) {
      $stmt->bindValue( $position++, (int) $trxId );
      $stmt->bindValue( $position++, $key );
      $stmt->bindValue( $position++, $val );
    }

    $stmt->execute();
  }
}
?>

