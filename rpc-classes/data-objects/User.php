<?php

/**
 *  @brief Represents a user.
 *
 *  @ingroup dos
 */
class User extends GenericDataObject {
  public $id;
  public $deleted;
  public $login;
  public $fullName;

  public function __construct( $data ) {
    parent::__construct();

    $this->id = (int) $data[ 'userID' ];
    $this->deleted = (bool) $data[ 'userDeleted' ];
    $this->login = $data[ 'userLogin' ];
    $this->fullName = $data[ 'userFullName' ];
  }
}

?>
