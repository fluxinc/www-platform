<?php

/**
 *  @defgroup Services Services
 *  @brief Public web services.
 *
 *  This module contains all services that are called by user interfaces in
 *  flash and javascript through amfphp package.
 *
 *  @ingroup Helpers
 */

/**
 *  @brief Defines methods shared between services.
 *
 *  @ingroup Services
 */
class GenericService extends GenericHelper {
  protected $serviceName = null;
  protected $methodsRoles = array();

  public function __construct( ServiceCaller $caller ) {
    $this->serviceName = get_class( $this );

    if (
      ! $caller instanceof ServiceCaller ||
      ! $caller->callerIsAvailable()
    ) {
      throw new ApplicationException( 'service-constructed-outside-caller' );
    }
  }

  /**
   *  @brief Checks if current user is allowed to call method from this service.
   *
   *  @param $methodName Name of the method to check.
   *
   *  @return Bool.
   */
  public function isAllowedToCall( $methodName ) {
    if ( 'isAllowedToCall' === $methodName ) {
      return true;
    }

    if ( ! isset( $this->methodsRoles[ $methodName ] ) ) {
      throw new WrongDataException( 'isallowedtocall-methodname' );
    }

    $roles = $this->methodsRoles[ $methodName ];

    if ( $roles === '' ) {
      return true;
    }
    else {
      return (bool) Authenticate::isUserInRole( $roles );
    }
  }

  /**
   *  @brief Returns id of currently logged in user.
   *
   *  You should first make sure that some user is logged in before calling this
   *  method. Usually this is done witha a call to assertRole().
   *
   *  @return Int id.
   */
  protected function userId() {
    return (int) Authenticate::getAuthUser();
  }

  protected function assertRole( $role, $source = null ) {
    if ( ! Authenticate::isUserInRole( $role ) ) {
      if ( ! isset( $source ) )
        $source = strtolower( $this->serviceName ) . '-service';

      throw new PermissionException( $source );
    }
  }

  /**
   *  @brief Converts array of rows to array of data objects.
   */
  protected function rowsToDataObjects( $objectType, $rows ) {
    $return = array();
    foreach( $rows as $row ) {
      $return[] = new $objectType( $row );
    }
    return $return;
  }

  /**
   *  @name Format changing methods
   *
   *  Those methods can be used to reformat data between what service gets
   *  from database and what client expects, also reverse.
   *
   *  @{
   */

  /**
   *  @brief Format date time received from client to what db expects.
   *
   *  This asserts that date is correct and if it isn't throws
   *  WrongDataException.
   */
  protected function clientDatetimeToDbFormat( $date, $source = null ) {
    $date = (string) $date;

    $this->sqlDatetimeToTimestamp( $date, $source );
    return $date;
  }

  /**
   *  @brief Format date received from client to what db expects.
   *
   *  This asserts that date is correct and if it isn't throws
   *  WrongDataException.
   */
  protected function clientDateToDbFormat( $date, $source = null ) {
    $date = (string) $date;

    $this->sqlDateToTimestamp( $date, $source );
    return $date;
  }
  ///@}

  /**
   * @brief Tries to load file contents into PublishedFile object.
   */
  protected function getFileContents( $fullPath, $fileName, $error ) {
    if ( ! file_exists( $fullPath ) ) {
      throw new NotFoundException( 'file' );
    }

    $return = @file_get_contents( $fullPath );
    if ( ! $return ) {
      throw new ApplicationException( $error );
    }

    return new PublishedFile( $fileName, $return );
  }
}
?>
