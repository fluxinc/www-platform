<?php

/**
 *  @brief Used to call procedures provided by Services.
 *
 *  @ingroup Helpers
 */
class ServiceCaller extends GenericHelper {
  private $service;
  private $serviceName;
  private $lastCallReturnedError = false;

  /**
   *  @brief Prepares instance used as a proxy to service object.
   */
  public function __construct( $serviceName ) {
    if ( ! file_exists( SERVICES_PATH . $serviceName . '.php' ) ) {
      throw new ApplicationException( 'servicecaller-service-not-found' );
    }

    require_once( SERVICES_PATH . $serviceName . '.php' );

    $this->serviceName = $serviceName;
    $this->service = new $serviceName( $this );

    if ( empty( $this->service ) ) {
      throw new ApplicationException( 'servicecaller-init-failed' );
    }
  }

  /**
   *  @brief Tells whether this ServiceCaller instance has already been
   *  initialized.
   *
   *  @return True if caller's initialization hasn't yet finished.
   */
  public function callerIsAvailable() {
    return empty( $this->service );
  }

  /**
   *  @brief Can be used to check if last call to a service returned Error.
   *
   *  @return Boolean - true if last call resulted in Error.
   */
  public function returnedError() {
    return $this->lastCallReturnedError;
  }

  /**
   *  @brief Performs a call to service's method
   *
   *  @return Mixed - value received from call
   */
  public function __call( $method, $args ) {
    ErrorHandler::register();

    try {
      if ( ! method_exists( $this->service, $method ) )
        throw new ApplicationException( 'servicecaller-unknown-method' );

      $transactionName = strtolower( $this->serviceName . '-' . $method );

      if ( ! $this->service->isAllowedToCall( $method ) ) {
        throw new PermissionException( $transactionName );
      }

      $return = $this->encapsulateTransaction(
        array( $this->service, $method ), $args, $transactionName
      );
      if ( $return instanceof Error )
        $this->lastCallReturnedError = true;
      else
        $this->lastCallReturnedError = false;

      ErrorHandler::restoreDefault();
      return $return;
    }
    catch ( Exception $e ) {
      $return = $this->catchException( $e );
      if ( $return instanceof Error )
        $this->lastCallReturnedError = true;
      else
        $this->lastCallReturnedError = false;

      ErrorHandler::restoreDefault();
      return $return;
    }
  }

  /**
   *  @brief Catches exceptions and returns appropriate Error data objects.
   */
  protected function catchException( $e ) {
    if ( FLUX_INC_DEBUG_MODE === true ) {
      throw $e;
    }

    $defaultSource = strtolower( $this->serviceName ) . '-service';
    if ( $e instanceof FluxIncException && ! $e->getMessage() ) {
      $e->setMessage( $defaultSource );
    }

    if ( $e instanceof PermissionException ) {
      return new Error( Error::TYPE_PERMISSION, $e->getMessage() );
    }
    elseif ( $e instanceof DatabaseException ) {
      return new Error( Error::TYPE_DATABASE, $e->getMessage() );
    }
    elseif ( $e instanceof NetworkException ) {
      return new Error( Error::TYPE_NETWORK, $e->getMessage() );
    }
    elseif ( $e instanceof DuplicateException ) {
      return new Error( Error::TYPE_DUPLICATE, $e->getMessage() );
    }
    elseif ( $e instanceof NotFoundException ) {
      return new Error( Error::TYPE_NOT_FOUND, $e->getMessage() );
    }
    elseif ( $e instanceof DeadlockException ) {
      return new Error( Error::TYPE_DATABASE_JAMMED, $e->getMessage() );
    }
    elseif ( $e instanceof LockWaitTimeoutException ) {
      return new Error( Error::TYPE_DATABASE_JAMMED, $e->getMessage() );
    }
    elseif ( $e instanceof WrongDataException ) {
      return new Error( Error::TYPE_WRONG_DATA, $e->getMessage() );
    }

    self::logError(
      "Caught unknown exception.\n%s\n%s",
      $e->getMessage(),
      $e->getTraceAsString()
    );

    return new Error();
  }
}

?>
