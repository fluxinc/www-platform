<?php

class FilesPublisher {
  private static $types = array(
    'jpg' => 'image/jpeg',
    'gif' => 'image/gif',
    'png' => 'image/png',
    'svg' => 'image/svg+xml',
    'pdf' => 'application/pdf',
    'wav' => 'audio/x-wav',
    'mp3' => 'audio/mpeg',
    'aac' => 'audio/x-aac'
  );

  public static function getFile( $service, $method /* variable arguments */ ) {
    $result = new Error();

    try {
      if ( empty( $service ) || empty( $method ) ) {
        throw new ApplicationException( 'filespublisher-wrong-arguments' );
      }

      $callback = array( new ServiceCaller( $service ), $method );
      $arguments = func_get_args();
      array_shift( $arguments );
      array_shift( $arguments );

      $result = call_user_func_array( $callback, $arguments );
    }
    catch ( Exception $e ) {
      // result already is an error
    }

    if ( ! $result instanceof PublishedFile && ! $result instanceof Error ) {
      $result = new Error();
    }

    if ( $result instanceof Error ) {
      if ( 'application' === $result->type ) {
        header( 'HTTP/1.1 500 Internal Server Error' );
        die( $result->type . '(' . $result->source . ')' );
      }
      elseif ( 'permission' === $result->type ) {
        header( 'HTTP/1.1 403 Forbidden' );
        die( 'Forbidden.' );
      }
      else {
        header( 'HTTP/1.1 404 Not Found' );
        die( 'File not found.' );
      }
    }
    else {
      if ( ! empty( self::$types[ $result->extension ] ) ) {
        header( 'Content-type: ' . self::$types[ $result->extension ] );
      }
      header(
        'Content-Disposition: inline; filename="' . $result->name . '"'
      );
      header( 'Expires: 0' );
      header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
      header( 'Pragma: no-cache' );

      echo( $result->contents );
    }
  }
}

class PublishedFile {
  public $name;
  public $extension;
  public $contents;

  public function __construct( $name, $contents ) {
    $this->name = $name;
    $this->extension = substr( $name, strrpos( $name, '.' ) + 1 );
    $this->contents = $contents;
  }
}

?>
