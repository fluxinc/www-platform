<?php

/**
 *  @defgroup Mgmt Data Managment
 *
 *  @brief Data manipulation api.
 *
 *  This module provides data manipulation layers. It does not care of things
 *  like permissions or handling errors, but if something goes wrong it will
 *  throw one of it's exceptions.
 *
 *  It's api is sql free.
 *
 *  @ingroup Helpers
 */

/**
 *  @brief Provides methods shared between different managers.
 *
 *  @ingroup Mgmt
 */
class GenericMgmt extends GenericHelper {
  ///@privatesection
  protected $dbh;

  public function __construct() {
    $this->dbh = new FluxIncPDO();
  }

  /**
   *  @brief Deprecated method. Simply use $stmt->execute().
   */
  protected function assertExecution(
    FluxIncPDOStatement $stmt, $source = null
  ) {
    $stmt->execute();
  }

  protected function assertRows( FluxIncPDOStatement $stmt, $source = null ) {
    if ( ! $stmt->rowCount() )
      throw new NotFoundException( $source );
  }

  /**
   *  @brief Begins transaction with currently opened connection.
   *
   *  @return Boolean.
   */
  public function beginTransaction() {
    return $this->dbh->beginTransaction();
  }

  /**
   *  @brief Commits transaction with currently opened connection.
   *
   *  @return Boolean.
   */
  public function commit() {
    return $this->dbh->commit();
  }

  /**
   *  @brief Rolls back transaction with currently opened connection.
   *
   *  @return Boolean.
   */
  public function rollback() {
    return $this->dbh->rollback();
  }
}

?>
