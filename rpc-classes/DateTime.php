<?php

/**
 * @brief Backport DateTime class to php 5.1
 */

class DateTime {
  private $epoch;

  public function __construct( $timeString = 'now' ) {
    $epoch = strtotime( $timeString );
    if ( false === $epoch ) {
      throw new ApplicationException( 'datetime-construct-wrong-parameter' );
    }
    $this->epoch = $epoch;
  }

  public function format( $format ) {
    return date( $format, $this->epoch );
  }

  public function modify( $timeString ) {
    $epoch = strtotime( $timeString, $this->epoch );
    if ( false === $epoch ) {
      throw new ApplicationException( 'datetime-modify-wrong-parameter' );
    }
    $this->epoch = $epoch;
  }

  public function setDate( $year, $month, $day ) {
    if ( ! checkdate( $month, $day, $year ) )
      throw new ApplicationException( 'datetime-setdate-wrong-parameter' );
    $epoch = mktime(
      date( 'H', $this->epoch ),
      date( 'i', $this->epoch ),
      date( 's', $this->epoch ),
      $month, $day, $year
    );
    if ( false === $epoch )
      throw new ApplicationException( 'datetime-setdate-wrong-parameter' );
    $this->epoch = $epoch;
  }

  public function setTime( $hour, $minute, $second = 0 ) {
    $epoch = mktime(
      $hour, $minute, $second,
      date( 'n', $this->epoch ),
      date( 'j', $this->epoch ),
      date( 'Y', $this->epoch )
    );
    if ( false === $epoch )
      throw new ApplicationException( 'datetime-settime-wrong-parameter' );
    $this->epoch = $epoch;
  }

  /**
   * @todo Code better handling of years having 53 weeks.
   */
  public function setISODate( $year, $week, $day = 1 ) {
    $year = (int) $year;
    $week = (int) $week;
    $day = (int) $day;

    $yearOk = $year > 1969 && $year < 2038;
    $dayOk = $day > 0 && $day < 8;

    if ( $this->yearHas53Weeks( $year ) ) {
      $weekOk = $week > 0 && $week < 54;
    }
    else {
      $weekOk = $week > 0 && $week < 53;
    }

    if ( ! $yearOk || ! $dayOk || ! $weekOk )
      throw new ApplicationException( 'datetime-setisodate-wrong-parameter' );

    // move to first day of first week
    $this->setDate( $year, 1, 1 );
    $dayOfWeek = date( 'N', $this->epoch );
    if ( $dayOfWeek <= 4 ) {
      if ( $dayOfWeek > 1 ) {
        --$dayOfWeek;
        $this->epoch = strtotime( '-' . $dayOfWeek . 'day', $this->epoch );
      }
    }
    else {
      $dayOfWeek = 8 - $dayOfWeek;
      $this->epoch = strtotime( '+' . $dayOfWeek . 'day', $this->epoch );
    }

    --$week;
    if ( $week ) {
      $this->epoch = strtotime( '+' . $week . 'week', $this->epoch );
    }

    --$day;
    if ( $day ) {
      $this->epoch = strtotime( '+' . $day . 'day', $this->epoch );
    }
  }

  private function yearHas53Weeks( $year ) {
    $time = mktime( 1, 1, 1, 1, 1, (int) $year );
    $dayOfWeek = (int) date( 'N', $time );

    if ( date( 'L', $time ) ) { // leap year
      if ( 3 === $dayOfWeek || 4 === $dayOfWeek )
        return true;
      else
        return false;
    }
    else { // not leap year
      if ( 4 === $dayOfWeek )
        return true;
      else
        return false;
    }
  }
}

?>
