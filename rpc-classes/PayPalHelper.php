<?php

class PayPalHelper extends GenericHelper {
  private static $responses = array();

  private $mgr;

  public function __construct() {
    $this->mgr = new PayPalMgmt();
  }

  public function getId( $post ) {
    $this->assertVerifiedNotification( $post );
    $this->assertCorrectRecipientAndCurrency( $post );

    $id = $this->mgr->insertTransaction(
      $post[ 'txn_id' ],
      $post[ 'item_name' ],
      $post[ 'txn_type' ],
      $post[ 'payment_status' ],
      $post[ 'mc_gross' ]
    );

    $this->mgr->insertDetails( $id, $post );

    return $id;
  }

  /**
   * @brief Posts a response to paypal to verify notification.
   */
  public function assertVerifiedNotification( $post ) {
    $response = $this->getResponseString( $post );
    $hash = md5( $response );

    if ( isset( self::$responses[ $hash ] ) ) {
      if ( self::$responses[ $hash ] ) {
        return true;
      }
      else {
        throw new WrongDataException( 'paypal-notification' );
      }
    }

    if ( ! empty( $post[ 'test_ipn' ] ) ) {
      $host = 'www.sandbox.paypal.com';
    }
    else {
      $host = 'www.paypal.com';
    }

    $header  = "POST /cgi-bin/webscr HTTP/1.0\r\n";
    $header .= "Host: $host:443\r\n";
    $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
    $header .= "Content-Length: " . strlen( $response ) . "\r\n\r\n";

    $fp = fsockopen( "ssl://$host", 443, $errno, $errstr, 30 );

    if ( ! $fp ) {
      throw new ApplicationException( 'cant-connect-to-paypal' );
    }
    fputs( $fp, $header . $response );

    $readingHeader = true;
    $wholeResponse = '';
    $firstLine = '';
    while ( ! feof( $fp ) ) {
      $line = trim( fgets( $fp, 1024 ) );

      if ( $readingHeader && empty( $line ) ) {
        $readingHeader = false;
      }
      elseif ( ! $readingHeader && ! $firstLine ) {
        $firstLine = $line;
      }

      $wholeResponse .= $line . "\n";
    }
    fclose( $fp );

    if ( 'VERIFIED' === $line ) {
      self::$responses[ $hash ] = true;
      return true;
    }
    elseif ( 'INVALID' === $line ) {
      self::$responses[ $hash ] = false;
      self::logError(
        "Received invalid paypal notification. Response was:\n%s",
        $response
      );
      throw new WrongDataException( 'paypal-notification' );
    }
    else {
      self::logError(
        "Received unexpected message from paypal. Sent:\n%s\n\nReceived:\n%s",
        $response, $wholeResponse
      );
      throw new ApplicationException( 'unexpected-message-from-pay-pal' );
    }
  }

  private function assertCorrectRecipientAndCurrency( $post ) {
    if ( PAYPAL_EMAIL !== $post[ 'receiver_email' ] ) {
      self::logError(
        'Received paypal notification for unknown recipient.'
      );
      throw new WrongDataException( 'paypal-email' );
    }
    if ( PAYPAL_CURRENCY !== $post[ 'mc_currency' ] ) {
      self::logError(
        'Received paypal notification for unaccepted currency.'
      );
      throw new WrongDataException( 'paypal-currency' );
    }
  }

  private function getResponseString( $post ) {
    $response = 'cmd=_notify-validate';

    foreach ( $_POST as $key => $value ) {
      if ( ! empty( $value ) ) {
        $value = urlencode( $value );
        $response .= "&$key=$value";
      }
    }

    return $response;
  }
}

?>
