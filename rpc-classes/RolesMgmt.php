<?php

/**
 * @brief Performs database operations related to roles.
 * @ingroup Mgmt
 */
class RolesMgmt extends GenericMgmt {
  ///@privatesection

  /**
   * @name Internal queries
   * @{
   */
  const QUERY_GET_BY_ID =
    'SELECT * FROM %TPXRoles WHERE roleID = :id LIMIT 1';
  const QUERY_GET_BY_NAME =
    'SELECT * FROM %TPXRoles WHERE roleName = :roleName LIMIT 1';
  const QUERY_GET_ALL =
    'SELECT * FROM %TPXRoles';
  const QUERY_GET_ASSIGNED_TO_USER =
    'SELECT roleID, roleName FROM %TPXUsersRoles NATURAL JOIN %TPXRoles
    WHERE userID = :user';
  const QUERY_ASSIGN_TO_USER =
    'INSERT IGNORE INTO %TPXUsersRoles ( userID, roleID )
    VALUES ( :userID, :roleID )';
  const QUERY_REMOVE_ASSIGNED_TO_USER =
    'DELETE FROM %TPXUsersRoles WHERE userID = :user AND roleID = :role';
  const QUERY_REMOVE_ALL_ASSIGNED_TO_USER =
    'DELETE FROM %TPXUsersRoles WHERE userID = :user';
  const QUERY_CREATE =
    'INSERT INTO %TPXRoles (roleName) VALUES (:roleName)';
  const QUERY_DELETE =
    'DELETE FROM %TPXRoles WHERE roleName = :roleName';
  ///@}


  public function __construct() {
    parent::__construct();
  }

  /**
   * @brief Fetches role by it's id.
   * @param $roleId Role id.
   * @return Row.
   */
  public function getInfoById( $roleId ) {
    $stmt = $this->dbh->prepare( self::QUERY_GET_BY_ID );
    $stmt->bindValue( ':id', (int) $roleId );
    $stmt->execute();
    $this->assertRows( $stmt );
    return $stmt->fetch();
  }

  /**
   * @brief Fetches role by it's name.
   * @param $roleName Role's name.
   * @return Row.
   */
  public function getInfoByName( $roleName ) {
    $stmt = $this->dbh->prepare( self::QUERY_GET_BY_NAME );
    $stmt->bindValue( ':roleName', (string) $roleName );
    $stmt->execute();
    $this->assertRows( $stmt );
    return $stmt->fetch();
  }

  /**
   * @brief Checks whether role with given name exists.
   * @param $roleName Name to check.
   * @return True if it exists false otherwise.
   */
  public function exists($roleName) {
    $stmt = $this->dbh->prepare( self::QUERY_GET_BY_NAME );
    $stmt->bindValue(':roleName', $roleName);
    $stmt->execute();
    return (bool) $stmt->rowCount();
  }

  /**
   * @brief Returns all defined roles.
   * @return Array of rows.
   */
  public function getAll() {
    $stmt = $this->dbh->prepare( self::QUERY_GET_ALL );
    $stmt->execute();
    return $stmt->fetchAll();
  }

  /**
   * @brief Returns all roles that have been assigned to user.
   * @param @userId User id.
   * @return Array of rows.
   */
  public function getAssignedToUser( $userId ) {
    $stmt = $this->dbh->prepare( self::QUERY_GET_ASSIGNED_TO_USER );
    $stmt->bindValue( ':user', (int) $userId );
    $stmt->execute();
    return $stmt->fetchAll();
  }

  /**
   * @brief Assigns user a role.
   * @param $userId User ID.
   * @param $roleId Id of role to be assigned.
   */
  public function assignToUser( $userId, $roleId ) {
    $stmt = $this->dbh->prepare( self::QUERY_ASSIGN_TO_USER );
    $stmt->bindValue(':userID', (int) $userId);
    $stmt->bindValue(':roleID', (int) $roleId );
    $stmt->execute();
  }

  /**
   * @brief Removes role assigned to user.
   * @param $userId User ID.
   * @param $roleId Id of role to be removed.
   */
  public function removeAssignedToUser( $userId, $roleId ) {
    $stmt = $this->dbh->prepare( self::QUERY_REMOVE_ASSIGNED_TO_USER );
    $stmt->bindValue(':user', (int) $userId);
    $stmt->bindValue(':role', (int) $roleId );
    $stmt->execute();
  }

  /**
   * @brief Remove all roles assigned to given user.
   * @param $userId User ID.
   */
  public function removeAllAssignedToUser( $userId ) {
    $stmt = $this->dbh->prepare( self::QUERY_REMOVE_ALL_ASSIGNED_TO_USER );
    $stmt->bindValue(':user', (int) $userId );
    $stmt->execute();
  }

  /**
   * @brief Adds new role to database.
   * @param $roleName Role's name.
   * @return Id of new role.
   */
  public function create( $roleName ) {
    $stmt = $this->dbh->prepare( self::QUERY_CREATE );
    $stmt->bindValue( ':roleName', (string) $roleName );
    $stmt->execute();
    return (int) $this->dbh->lastInsertId();
  }

  /**
   * @brief Deletes a role.
   * @param $roleName role name
   */
  public function delete( $roleName ) {
    $stmt = $this->dbh->prepare( self::QUERY_DELETE );
    $stmt->bindValue( ':roleName', $roleName );
    $stmt->execute();
  }
}

?>
