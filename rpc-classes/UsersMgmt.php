<?php

/**
 * @brief Performs database operations related to users.
 * @ingroup Mgmt
 */
class UsersMgmt extends GenericMgmt {
  ///@privatesection

  /**
   * @name Internal queries
   * @{
   */
  const QUERY_USER_WITH_ID_EXISTS =
    'SELECT userID FROM %TPXUsers WHERE userID = :id';
  const QUERY_USER_WITH_LOGIN_EXISTS =
    'SELECT userLogin FROM %TPXUsers WHERE userLogin = :login';
  const QUERY_INSERT_NEW_USER =
    'INSERT INTO %TPXUsers
    ( userDeleted, userLogin, userFullName, userPasswordHash )
    VALUES ( 0, :login, :name, :hash )';
  const QUERY_GET_BY_LOGIN =
    'SELECT * FROM %TPXUsers WHERE userLogin = :login LIMIT 1';
  const QUERY_GET_BY_ID =
    'SELECT * FROM %TPXUsers WHERE userID = :id LIMIT 1';
  const QUERY_GET_ALL =
    'SELECT * FROM %TPXUsers WHERE userDeleted <=> :deleted ORDER BY userLogin';
  const QUERY_CHANGE_PASSWORD =
    'UPDATE %TPXUsers SET userPasswordHash = :hash WHERE userID=:user LIMIT 1';
  const QUERY_CHANGE_DETAILS =
    'UPDATE %TPXUsers SET userFullName = :name WHERE userID = :user LIMIT 1';
  const QUERY_SET_DELETED =
    'UPDATE %TPXUsers SET userDeleted = :value WHERE userID = :user';
  ///@}


  public function __construct() {
    parent::__construct();
  }

  /**
   * @brief Checks whether user with given id exists.
   * @param $id Id to check.
   * @return True if it exists false otherwise.
   */
  public function userWithIdExists( $id ) {
    if ( ! is_numeric( $id ) ) {
      throw new WrongDataException( 'user-id' );
    }
    $stmt = $this->dbh->prepare( self::QUERY_USER_WITH_ID_EXISTS );
    $stmt->bindValue( ':id', (int) $id );
    $stmt->execute();
    return (bool) $stmt->rowCount();
  }

  /**
   * @brief Checks whether user with given login exists.
   * @param $login Login to check.
   * @return True if it exists false otherwise.
   */
  public function userWithLoginExists( $login ) {
    $stmt = $this->dbh->prepare( self::QUERY_USER_WITH_LOGIN_EXISTS );
    $stmt->bindValue( ':login', (string) $login );
    $stmt->execute();
    return (bool) $stmt->rowCount();
  }

  /**
   * @brief Fetches user info by id.
   * @param $id User id used for search.
   * @return Row.
   */
  public function getInfoById( $id ) {
    $stmt = $this->dbh->prepare( self::QUERY_GET_BY_ID );
    $stmt->bindValue( ':id', (int) $id );
    $stmt->execute();
    $this->assertRows( $stmt );
    return $stmt->fetch();
  }

  /**
   * @brief Fetches user info by login.
   * @param $login Username used for search.
   * @return Row.
   */
  public function getInfoByLogin( $login ) {
    $stmt = $this->dbh->prepare( self::QUERY_GET_BY_LOGIN );
    $stmt->bindValue( ':login', (string) $login );
    $stmt->execute();
    $this->assertRows( $stmt );
    return $stmt->fetch();
  }

  /**
   * @brief Adds new user to database.
   * @param $login Login of new user.
   * @param $name Full name.
   * @param $hash Password's hash.
   * @return Int id of new user.
   */
  public function createNew( $login, $name, $hash ) {
    $stmt = $this->dbh->prepare( self::QUERY_INSERT_NEW_USER );
    $stmt->bindValue( ':login', (string) $login );
    $stmt->bindValue( ':name', (string) $name );
    $stmt->bindValue( ':hash', (string) $hash );
    $stmt->execute();
    return (int) $this->dbh->lastInsertId();
  }

  /**
   * @brief Lists all users.
   * @param $deleted Whether to return existing (default) or deleted users.
   * @return Array of rows.
   */
  public function getAll( $deleted = false ) {
    $stmt = $this->dbh->prepare( self::QUERY_GET_ALL );
    $stmt->bindValue( ':deleted', (bool) $deleted );
    $stmt->execute();
    return $stmt->fetchAll();
  }

  /**
   *  @brief Changes users password.
   */
  public function changeUsersPassword( $userId, $pass ) {
    $stmt = $this->dbh->prepare( self::QUERY_CHANGE_PASSWORD );
    $stmt->bindValue( ':user', (int) $userId );
    $stmt->bindValue( ':hash', (string) $pass );
    $stmt->execute();
  }

  /**
   *  @brief Changes users details.
   */
  public function changeUsersDetails( $userId, $name ) {
    $stmt = $this->dbh->prepare( self::QUERY_CHANGE_DETAILS );
    $stmt->bindValue( ':user', (int) $userId );
    $stmt->bindValue( ':name', (string) $name );
    $stmt->execute();
  }

  /**
   *  @brief Changes delete status of a user.
   */
  public function setDeleted( $userId, $delete ) {
    $stmt = $this->dbh->prepare( self::QUERY_SET_DELETED );
    $stmt->bindValue( ':user', (int) $userId );
    $stmt->bindValue( ':value', (bool) $delete );
    $stmt->execute();
  }
}

?>
