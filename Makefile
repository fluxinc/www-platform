APP_SHORT=some_name
PRIVATE_DIR=rpc-classes/
SERVICE_DIR=services/$(APP_SHORT)/
PRIVATE_FILES=$(shell find $(PRIVATE_DIR) | grep -v '\.svn' | grep -v '/$$')
SERVICE_FILES=$(shell find $(SERVICE_DIR) | grep -v '\.svn' | grep -v '/$$')
MYSQL_CMD=mysql -u $(APP_SHORT)user -ptestpwd
DB_NAME="$(APP_SHORT)Db"
OVRDBG=global-headers/override-debug.inc.php

default: check-syntax

dbg-on:
	@echo '<?php' > $(OVRDBG)
	@echo 'ifndefdefine("FLUX_INC_DEBUG_MODE",true);' >> $(OVRDBG)
	@echo '?>' >> $(OVRDBG)
	@echo "Debugging turned on."

dbg-off:
	@rm -f $(OVRDBG)
	@echo "Debugging turned off."

check-syntax:
	@( tools/parse $(PRIVATE_FILES) $(SERVICE_FILES) && echo "Syntax Ok" ) || \
	  ( (echo "Syntax error") && ( exit 1 ) )

svn-properties:
	tools/set-svn-ignore
	tools/set-svn-eol-style

config-edit:
	@tools/init-config - "${APP_SHORT}" "`pwd`"

config-reset:
	@tools/init-config rf "${APP_SHORT}" "`pwd`"

doc: doc/doxygen

doc/doxygen: $(PRIVATE_FILES) $(SERVICE_FILES) doc/documentation.php \
			 doc/Doxyfile .revision
	@( cat doc/Doxyfile; echo 'PROJECT_NUMBER = "R$(shell cat .revision)"' ) | \
	  doxygen - > /dev/null
	@echo "Html documentation updated."

doc-pdf: doc/reference.pdf

doc/reference.pdf: doc/doxygen
	cp doc/latex-makefile doc/doxygen/latex/Makefile
	cp doc/latex-missing-pspicture.ps doc/doxygen/latex/pspicture.ps
	cd doc/doxygen/latex && $(MAKE)
	cp doc/doxygen/latex/refman.pdf doc/reference.pdf

private-tags:
	ctags -R $(PRIVATE_DIR) $(SERVICE_DIR)

.revision: force
	@[ -f ".revision" ] || \
	  echo -n "empty" > .revision
	@[ `svnversion -n . | tr -d 'M' | sed -e 's/:.*//'` \
	  = `cat .revision` ] || \
	  svnversion -n . | tr -d 'M' | sed -e 's/:.*//' > .revision

db:
	@echo "DROP SCHEMA IF EXISTS \`$(DB_NAME)\`;" | $(MYSQL_CMD)
	@echo "Delete old database."
	@echo "CREATE DATABASE \`$(DB_NAME)\`;" | $(MYSQL_CMD)
	@echo "Created empty database."
	@cat doc/db/tables.sql | $(MYSQL_CMD) $(DB_NAME)
	@echo "Created database."
	@cat doc/db/inserts.sql | $(MYSQL_CMD) $(DB_NAME)
	@echo "Inserted initial rows."

force:

