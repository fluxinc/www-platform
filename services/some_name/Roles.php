<?php

/**
 * @brief Provides methods related to roles management.
 *
 * @details Beside user created roles there are two roles used by the system
 * internally. One is "user" and describes every logged in user and
 * complementary to it is the role called "guest".
 *
 * @ingroup Services
 */
class Roles extends GenericService {
  ///@privatesection
  protected $methodsRoles = array(
    'getOwn'                  => 'user',
    'getAll'                  => 'admin',
    'getAssignedToUser'       => 'admin',
    'assignToUser'            => 'admin',
    'removeAssignedToUser'    => 'admin',
    'removeAllAssignedToUser' => 'admin',
    'roleMatchesPattern'      => 'superadmin',
    'exists'                  => 'superadmin',
    'create'                  => 'superadmin',
    'delete'                  => 'superadmin'
  );

  const PATTERN_ROLE_NAME = '/^[a-zA-Z0-9\-]{3,20}$/';

  private $mgr = null;

  private $reservedRoles = array( 'user', 'guest' );

  /**
   * @brief Prepares service caller and data manager.
   */
  public function __construct( ServiceCaller $caller ) {
    parent::__construct( $caller );

    $this->mgr = new RolesMgmt();
  }

  /**
   * @brief Lists all user-defined roles.
   * @return Array containing all roles defined in system (strings).
   */
  public function getAll() {
    $roles = $this->mgr->getAll();
    return $this->rowsToArrayOfRoles( $roles );
  }

  /**
   * @brief Lists roles assigned to current user.
   * @return Array of roles (strings).
   */
  public function getOwn() {
    $roles = $this->mgr->getAssignedToUser( $this->userId() );
    return $this->rowsToArrayOfRoles( $roles );
  }

  /**
   * @brief Lists roles assigned to user.
   * @param $userId Id of user to check.
   * @return Array of roles (strings).
   */
  public function getAssignedToUser( $userId ) {
    $roles = $this->mgr->getAssignedToUser( $userId );
    return $this->rowsToArrayOfRoles( $roles );
  }

  /**
   * @brief Assign a set of roles to a user.
   * @param $userId User ID.
   * @param $roles Comma separated list of roles.
   * @return True on success.
   */
  public function assignToUser( $userId, $roles ) {
    $roles = $this->csvToArray( $roles );

    if ( empty( $roles ) ) {
      throw new WrongDataException( 'roles-empty-roles-parameter' );
    }

    foreach ( $roles as $role ) {
      $roleId = $this->mgr->getInfoByName( $role );
      $roleId = $roleId[ 'roleID' ];
      $this->mgr->assignToUser( $userId, $roleId );
    }
    return true;
  }

  /**
   * @brief Remove a set of roles assigned to user.
   * @param $userId User ID.
   * @param $roles Comma separated list of roles.
   * @return True on success.
   */
  public function removeAssignedToUser( $userId, $roles ) {
    $roles = $this->csvToArray( $roles );

    if ( empty( $roles ) ) {
      throw new WrongDataException( 'roles-empty-roles-parameter' );
    }

    foreach ( $roles as $role ) {
      $roleId = $this->mgr->getInfoByName( $role );
      $roleId = $roleId[ 'roleID' ];
      $this->mgr->removeAssignedToUser( $userId, $roleId );
    }
    return true;
  }

  /**
   * @brief Remove all user's assigned roles.
   * @param $userId User ID.
   * @return True on success.
   */
  public function removeAllAssignedToUser( $userId ) {
    $this->mgr->removeAllAssignedToUser( $userId );
    return true;
  }

  /**
   * @brief Checks if string is correct role name.
   * @details Role name can consist of 3 - 20 alphanumeric characters and
   * hyphens.
   * @param $roleName Name to check.
   * @return Boolean true if name is correct role name, false otherwise.
   */
  public function roleMatchesPattern( $roleName ) {
    return (bool) preg_match( self::PATTERN_ROLE_NAME, $roleName );
  }

  /**
   * @brief Checks if a role with already exists.
   * @param $roleName Role to check.
   * @return True if role exists, false otherwise.
   */
  public function exists( $roleName ) {
    return (bool) $this->mgr->exists( $roleName );
  }

  /**
   * @brief Creates a role.
   * @param $roleName Name of the role to create.
   * @return True on success.
   */
  public function create( $roleName ) {
    $roleName = strtolower( trim( $roleName ) );

    if ( in_array( $roleName, $this->reservedRoles ) ) {
      throw new WrongDataException( 'roles-reserved-role-name' );
    }

    if ( ! $this->roleMatchesPattern( $roleName ) ) {
      throw new WrongDataException( 'roles-role-name' );
    }

    $this->mgr->create( $roleName );
    return true;
  }

  /**
   * @brief Deletes a role.
   * @param $roleName Role to delete.
   * @return true on success
   */
  public function delete( $roleName ) {
    $this->mgr->delete( $roleName );
    return true;
  }

  /**
   * @brief Convert a csv string into a valid array.
   * @details The difference from simple explode() call is that all values
   * are trimmed.
   * @param $csv Comma separated values.
   * @return Array with the extracted values.
   */
  private function csvToArray( $csv ) {
    $return = explode( ',', $csv );
    foreach ( $return as & $value ) {
      $value = trim( $value );
    }
    return $return;
  }

  private function rowsToArrayOfRoles( $rows ) {
    $return = array();
    foreach ( $rows as $row ) {
      $return[] = $row[ 'roleName' ];
    }
    return $return;
  }
}

?>
