<?php

/**
 * @brief Provides methods related to users, being one and managing other.
 * @ingroup Services
 */
class Users extends GenericService {
  ///@privatesection
  protected $methodsRoles = array(
    'isLoggedIn'              => '',
    'logIn'                   => '',
    'logout'                  => '',
    'loginMatchesPattern'     => '',
    'passwordMatchesPattern'  => '',
    'nameAlreadyUsed'         => '',
    'getCurrent'              => 'user',
    'changeOwnPassword'       => 'user',
    'changeOwnDetails'        => 'user',
    'register'                => 'admin',
    'getById'                 => 'admin',
    'getByLogin'              => 'admin',
    'getAll'                  => 'admin',
    'changeDetails'           => 'admin',
    'changePassword'          => 'superadmin',
    'setDeleted'              => 'superadmin'
  );

  /**
   * @brief Regular expression patterns
   * @{
   */
  const PATTERN_LOGIN = '/^\w{3,20}$/';
  const PATTERN_PASSWORD = '/^.{5,}$/';
  const PATTERN_FIRST_NAME = '/^[^\d\{\}\[\],_:]{2,}$/';
  const PATTERN_LAST_NAME = '/^[^\d\{\}\[\],_:]{2,}$/';
  ///@}

  private $mgr = null;

  /**
   * @brief Prepares service caller and data manager.
   */
  public function __construct( ServiceCaller $caller ) {
    parent::__construct( $caller );

    $this->mgr = new UsersMgmt();
  }

  /**
   * @brief Checks if there is a user logged in right now.
   * @roles guest
   * @return True if user is logged in.
   */
  public function isLoggedIn() {
    return (bool) Authenticate::isAuthenticated();
  }

  /**
   * @brief Gets info about currently logged in user.
   * @roles user
   * @return Object of class User.
   */
  public function getCurrent() {
    return new User( $this->mgr->getInfoById( $this->userId() ) );
  }

  /**
   * @brief Gets user with specified id.
   * @roles admin
   * @return Object of class User.
   */
  public function getById( $userId ) {
    return new User( $this->mgr->getInfoById( $userId ) );
  }

  /**
   * @brief Gets user with specified login.
   * @roles admin
   * @return Object of class User.
   */
  public function getByLogin( $login ) {
    return new User( $this->mgr->getInfoByLogin( $login ) );
  }

  /**
   * @brief Log's user in.
   * @param $login Username.
   * @param $password Password.
   * @roles guest
   * @return User instance if login was successful.
   */
  public function logIn( $login, $password ) {
    $info = $this->mgr->getInfoByLogin( $login );

    if ( sha1( $password ) !== $info[ 'userPasswordHash' ] ) {
      throw new WrongDataException( 'user-password' );
    }
    elseif ( $info[ 'userDeleted' ] ) {
      throw new PermissionException( 'user-login' );
    }
    else {
      $userId = (int) $info[ 'userID' ];
      $roles = 'user';

      $rolesMgr = new RolesMgmt();
      $rows = $rolesMgr->getAssignedToUser( $userId );
      foreach ( $rows as $row ) {
        $roles .= ',' . $row[ 'roleName' ];
      }

      Authenticate::login( (int) $info[ 'userID' ], $roles );

      return new User( $info );
    }
  }

  /**
   * @brief Logs user out.
   * @roles guest
   * @return True.
   */
  public function logout() {
    Authenticate::logout();
    return true;
  }

  /**
   * @brief Checks whether string is coorect login pattern.
   * @details It is correct if it has 3 to 20 alphanumeric or _ characters.
   * @roles guest
   * @return True if string is acceptable username.
   */
  public function loginMatchesPattern( $login ) {
    return (bool) preg_match( self::PATTERN_LOGIN, $login );
  }

  /**
   * @brief Checks whether password is correct password pattern.
   * @details It is correct if it has at least 5 chars, any chars.
   * @roles guest
   * @return True if string is acceptable password.
   */
  public function passwordMatchesPattern( $password ) {
    return (bool) preg_match( self::PATTERN_PASSWORD, $password );
  }

  /**
   * @brief Checks whether user with given login exists.
   * @param $login Login to check.
   * @roles guest
   * @return True if it exists false otherwise.
   */
  public function nameAlreadyUsed( $login ) {
    return $this->mgr->userWithLoginExists( $login );
  }

  /**
   * @brief Registers new user
   * @details Login must be 3 - 20 alphanumeric characters or underscore.
   * Password must be at least 5 characters long and can contain any chars.
   * Newly registered users can't log in until some admin assigns them a group.
   * @roles admin
   * @return Object of class User.
   */
  public function register( $login, $fullName, $password ) {
    if ( ! $this->loginMatchesPattern( $login ) ) {
      throw new WrongDataException( 'user-login' );
    }

    if ( ! $this->passwordMatchesPattern( $password ) ) {
      throw new WrongDataException( 'user-password' );
    }

    $id = $this->mgr->createNew( $login, $fullName, sha1( $password ) );
    return new User( $this->mgr->getInfoById( $id ) );
  }

  /**
   * @brief Gets list of all users.
   * @roles admin
   * @return Array of Users objects.
   */
  public function getAll( $deleted = false ) {
    $list = $this->mgr->getAll( $deleted );
    return $this->rowsToDataObjects( 'User', $list );
  }

  /**
   * @brief Changes deletion status of a user.
   * @param $userId Id of user who's delete flag should be changed.
   * @param $delete New value of delete flag. Set to true to delete user.
   * @roles superadmin
   * @return True on success.
   */
  public function setDeleted( $userId, $delete = true ) {
    $this->mgr->setDeleted( $userId, $delete );
    return true;
  }

  /**
   * @brief Changes detailed information of arbitrary user.
   * @roles admin
   * @return True on success.
   */
  public function changeDetails( $userId, $fullName ) {
    $this->mgr->changeUsersDetails( $userId, $fullName );
    return true;
  }

  /**
   * @brief Changes detailed information of currently logged in user.
   * @roles user
   * @return True on success.
   */
  public function changeOwnDetails( $fullName ) {
    $this->mgr->changeUsersDetails( $this->userId(), $fullName );
    return true;
  }

  /**
   * @brief Changes password of currently logged in user.
   * @param $newPassword Password which must match password pattern.
   * @roles user
   * @return True on success.
   */
  public function changeOwnPassword( $newPassword ) {
    if ( ! $this->passwordMatchesPattern( $newPassword ) ) {
      throw new WrongDataException( 'user-password' );
    }

    $this->mgr->changeUsersPassword( $this->userId(), sha1( $newPassword ) );
    return true;
  }

  /**
   * @brief Changes password of specified user.
   * @param $userId Id of user who's password should be changed.
   * @param $newPassword Password which must match password pattern.
   * @roles superadmin
   * @return True on success.
   */
  public function changePassword( $userId, $newPassword ) {
    if ( ! $this->passwordMatchesPattern( $newPassword ) ) {
      throw new WrongDataException( 'user-password' );
    }

    $this->mgr->changeUsersPassword( $userId, sha1( $newPassword ) );
    return true;
  }
}

?>
